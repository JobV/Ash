# Ash - Simple Home Automation AI

Ash is a simple home automation AI.

Ash is built on [Lita](lita.io) and has as main goal to make my life easier.
From the box it's built to integrate with Telegram, but it can be easily
adapted to work on any other chat system.

## Roadmap

- Rewrite Telegram Lita Adapter
- Authentication
- Cool stuff (see issues)